# test_merakiweb


## To run

pytest test_merakiweb.py


##  choice of test framework and other general test considerations for this web app

- pytest along with selenium for automating the web app

pytest framework is a popular and proven automation framework for automating tests.  It is very efficient to 
create test cases and easy to use.  Pytest in conjunction with selenium will allow us to be able to quickly generate various web app functional test cases by leverage selenium IDE Chrome extension to emulate user interaction with the web app

- with selenium, we are able to quickly emulate all the major browsers to ensure browner compatibility 

- would normally not checkin the api key itself in the source code, will be using a configuration file or some sort

- instead of usng nth-child CSS_SELECTOR, normally will be working with development team to identify web
element through id or some other unique name to indentify the web element

- User or generate incorrect api code to ensure the web app is not allowing http request without the correct api key



##  Imagine if this was a large app serving 100s of thousands of customers. Share your thoughts on
#testing and areas to consider for such an app

- If this is a large app service 100s of thousands of customers, we will need to leverage 
threading libraries along with requests and urllib3 to emulate thousands of customers simultaneously accesss 
this large web with each accessing different url.  Requests and urllib3 will be more efficient use of resources to check the load of the web app

- Will closely montior the http response code after load get to certain point like 1000 users or 1000 users

- will need to look at the web app log to ensure there is no error or warning or memory usage 

